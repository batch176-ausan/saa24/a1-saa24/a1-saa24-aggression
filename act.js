db.coursess.insertMany([
{
    name: "Iphone X", 
    price: 20000,
    isActive : true,
    instructor : "Sir Marty"
},

{
    name: "CSS 101 + Flexbox", 
    price: 21000,
    isActive : true,
    instructor : "Sir Marty"
},

{
     name: "Javascript 101", 
    price: 19000,
    isActive : true,
    instructor : "Ma'am Joy"
},

{
     name: "Git 101, IDE and ClI", 
    price: 19000,
    isActive : false,
    instructor : "Ma'am Joy"
},

{
     name: "React Js 101", 
    price: 25000,
    isActive : true,
    instructor : "Ma'am Miah"
}
])


// db.users.find({$or:[

//         {"firstname":{$regex: 'y',$options: '$i'}},

//         {"lastname":{$regex: 'y',$options: '$i'}}

//     ]},
//     {"_id":0,"email":1,"isAdmin":1})




//1.

    db.coursess.find(
    {$and:
[
        {instructor:"Sir Marty"},
        {price:{$gte:20000}}
]
},
{
    "_id": 0,
    "name": 1,
    "price": 1
}
)

    //2.

    db.coursess.find({$and:
[
        {instructor:"Ma'am Joy"},
        {isActive:false}
]
},
{
    "_id": 0,
    "name": 1,
    "price": 1
}
)
    
//3.
    db.coursess.find({$and:[

        {"name":{$regex: 'r',$options: '$i'}},

        {"price":{$gte: 25000}}

    ]})

    
//4.
    db.coursess.updateMany({$lt:2000},{$set:{"isActive":false}})

//5.
    db.coursess.deleteMany({$gt:25000})
